<?php

use App\Http\Controllers\AdminDashboardController;
use App\Http\Controllers\CartItemController;
use App\Http\Controllers\ProductController;
use App\Models\Cart;
use App\Models\Product;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('product', [ProductController::class,'index'])
	->name('product-listing')
	->middleware('auth');

Route::get('createproduct',[ProductController::class,'store'])->name('product.upload');
Route::post('createproduct',[ProductController::class,'storepost'])->name('product.upload.post');
Route::get('productdetail/{id}',[ProductController::class, 'detail'])->name('product.detail');
Route::delete('product/{id}', [ProductController::class, 'destroy'])->name('product.delete');
Route::get('product/{id}/edit', [ProductController::class, 'edit'])->name('product.edit');
Route::post('product/{id}', [ProductController::class, 'editpost'])->name('product.edit.post');

// Cart Item
Route::get('mycart/{id}', [CartItemController::class, 'myCart'])->name('mycart');
Route::post('addtocart',[CartItemController::class, 'addToCart'])->name('addtocart.post');
Route::get('getCartItemCount',[CartItemController::class, 'getCartItemCount'])->name('cartitem.count');
Route::post('removeCartItem', [CartItemController::class, 'removeCart'])->name('remove.cart');

//login
Route::view('dashboard', 'dashboard')
	->name('dashboard')
	->middleware(['auth', 'verified']);

Route::prefix('user')->middleware(['auth', 'verified'])->group(function () {
	Route::view('profile', 'profile.show');
});


//Grouping middleware

Route::middleware(['auth'])->group(function(){
	Route::get('createproduct',[ProductController::class,'store'])->name('product.upload');
});

//admin dashboard

Route::get('catalog',[AdminDashboardController::class, 'catalog'])->name('catalog.index');

