@extends('layout.main')

@section('content')


<section class="bg-light p-5">
    <div class="container ">

        <div id="message">
            <div class="alert d-none" role="alert" id="alert">
               
            </div>
        </div>
        <h4 class="text-center">Produk Detail</h1>

            <div class="d-sm-flex justify-content-center p-5">

                <img src="{{ asset('storage/images/'.$product->image)}}" class="img-fluid" alt="">

                <div class="desc " style="margin-left: 20px;">
                    <p><strong>Nama Produk:</strong></p>
                    <p>{{ $product->name}}</p>
                    <p><strong>Detail:</strong></p>
                    <p>{{ $product->desc}}</p>
                    <p><strong>Price:</strong></p>
                    <p>RM {{ $product->price}}</p>
                    <p><strong>Stock:</strong></p>
                    <p>{{ $product->quantity}}</p>
                    <p><strong>Quantity:</strong></p>

                    <div class="d-sm-flex">
                        <div class="input-group mb-3 ">
                            <input type="text" id="quantity" value="1" class="form-control">
                            <button class="btn btn-primary" onclick="add(1)">+</button>
                            <button class="btn btn-danger" onclick="minus(1)">-</button>

                        </div>


                    </div>
                    <button class="btn btn-primary" id="addToCart">Add To Cart</button>

                    <form class="d-inline" action="{{ route('product.delete',$product->id) }}" method="POST" onsubmit="return confirm('Are you sure to delete this product ' + '{{ $product->id }}') ">

                        @method('delete')
                        @csrf

                        <button type="submit" class="btn btn-danger">Delete</button>


                    </form>

                </div>
            </div>
            <input type="hidden" id="productId" value="{{ $product->id }}">
            <input type="hidden" id="authId" value="{{ Auth::id() }}">

    </div>


</section>

<script>
    $(document).ready(function() {
        $('#addToCart').click(function(e) {

            e.preventDefault();

            var formData = {
                "_token": "{{ csrf_token() }}",
                productId: $('#productId').val(),
                userId: $('#authId').val(),
                quantity: $('#quantity').val()
            }

            // console.log(formData);
            $.ajax({

                url: "{{ route('addtocart.post') }}",
                type: 'post',
                dataType: 'json',
                data: formData,



            }).done(function(data) {
                // console.log(data.status);
                console.log(data);

                if (data.status == "error") {
             

                    $('#alert').removeClass('d-none');
                    $('#alert').addClass('alert-danger');
                    $('#alert').html(data.type);

                } else {

                    $('#alert').removeClass('d-none');
                    $('#alert').removeClass('alert-danger');
                    $('#alert').addClass('alert-success');
                    $('#alert').html(data.type);

                    getCountCartItem();
                    
                }
            });


        });

        function getCountCartItem(){

            // var userId =  $('#authId').val();
            $.ajax({

                url:"{{ route('cartitem.count') }}",
                type: 'get',
                dataType: 'json',
                data: {
                    userId: $('#authId').val(),
                }
            }).done(function(data){

               
                $('#cartItemCount').text(data.count);
            })
        }
    })
</script>

<script>
    function add(num) {
        var quantity = parseInt(document.getElementById('quantity').value);

        var totalQuantity = quantity + num;

        document.getElementById('quantity').value = totalQuantity;
    }

    function minus(num) {
        var quantity = parseInt(document.getElementById('quantity').value);

        if (quantity != 0) {
            var totalQuantity = quantity - num;
            document.getElementById('quantity').value = totalQuantity;
        }


    }
</script>

@endsection