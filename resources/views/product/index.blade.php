@extends('layout.main')


@section('content')
<section class=" p-5 text-center text-sm-start ">
@if( session('success'))
<p class="alert alert-success">{{ session('success') }}</p>
@endif
    <div class="container">
        <div class="d-sm-flex align-items-center justify-content-between">
            <div class="">
                <h1>Din Store</h1>
                <p class="lead">
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Voluptas voluptate atque, assumenda obcaecati corporis provident?
                </p>
            </div>
            <div class="">
                <h1>Din Store</h1>
                <p class="lead">
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Voluptas voluptate atque, assumenda obcaecati corporis provident?
                </p>
            </div>
        </div>
    </div>

</section>

<!-- item section -->

<section class="p-5">
    <div class="container text-center text-start">
    <h5>Beli barang kedai kami</h5>
        <div class="row g-4">
            
       


            @foreach($products as $product)

            <div class="col" >
                <div class="card" >
                    <div class="card-body text-center">

                        <img src="{{ asset('storage/images/'.$product->image)}}" width="200" height="200"  alt="">


                       

                        <h5 class="card-tittle">
                            {{ $product->name }}
                        </h5>

                        <!-- <p class="card-text">
                            {{$product->desc}}
                        </p> -->

                        <a href="{{ route('product.detail',$product->id) }}" class="btn btn-primary">detail</a>
                    </div>
                </div>

            </div>

            @endforeach

        </div>
        <div class="pagination justify-content-center">
        {{$products->links()}}
        </div>
        
    </div>
</section>

<footer class="p-5 bg-dark text-center text-light position-relative ">
        <div class="container">
            <p class="lead">Copyright &copy; 2021 Nasruddin</p>
            <a href="#" class="position-absolute bottom-0 end-0 p-5">
               <i class="bi bi-arrow-up-circle h1"></i>
            </a>
        </div>
    </footer>






@endsection