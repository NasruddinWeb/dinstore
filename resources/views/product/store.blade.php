@extends('layout.main')

@section('content')

<div class="container">
<form action="{{ route('product.upload.post') }}" method="post" enctype="multipart/form-data">
    @csrf

    <h4>Insert Product</h4>
    <div class="row">
            @if ( $errors->any() )
            <p class="alert alert-danger">Please check your input</p>
            @endif
            
            @if( session('success') )
                    <p class="alert alert-success">{{ session('success') }}</p>
            @endif
        <div class="col-12 col-md-6">

           
            <div class="mb-3">
                <label for="name" class="form-label">Name</label>
                <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{ old('name') }}">

                @error('name')
                <div class="invalid-feedback">{{$message}}</div>
                @enderror
            </div>

            <div class="mb-3">
                <label for="desc" class="form-label">Description</label>
                <textarea class="form-control @error('desc') is-invalid @enderror"  id="desc" name="desc" rows="3" >{{old('desc')}}</textarea>

                @error('desc')
                <div class="invalid-feedback">{{$message}}</div>
                @enderror
            </div>

            {{--<div class="mb-3">
                <label for="category" class="form-label">Category</label>
                <select class="form-select" id="category" aria-label="Default select example">
                    <option selected>Open this select menu</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                </select>
            </div>--}}

            <div class="mb-3">
                <label for="price" class="form-label">Price</label>
                <input type="text" class="form-control @error('price') is-invalid @enderror" id="price" name="price" value="{{old('price')}}">

                @error('price')
                <div class="invalid-feedback">{{$message}}</div>
                @enderror
            </div>
        </div>
        <div class="col-12 col-md-6">
            <div class="mb-3">
                <label for="formFileSm" class="form-label">Product Picture</label>
                <input class="form-control form-control-sm @error('image') is-invalid @enderror" type="file" name="image" >

                @error('image')
                <div class="invalid-feedback">{{$message}}</div>
                @enderror
            </div>
        </div>

    </div>



    <button type="submit" class="btn btn-primary">Add Product Now</button>



</form>
</div>


@endsection