@extends('layout.main')


<style>
    .cart{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }
</style>

@section('content')

<!--Section: Block Content-->
<section class="container" style="margin-top: 100px;">

  <!--Grid row-->
  <div class="row g-4">

    <!--Grid column-->
    <div class="col-lg-8 ">

      <!-- Card -->
      <div class="mb-3 cart p-5">
        <div class="pt-4 ">

          <h5 class="mb-4">Cart (<span>2</span> items)</h5>
          

         @foreach($cart as $myCart)
          <div class="row mb-4" id="{{ $myCart['id'] }}">
            <div class="col-md-5 col-lg-3 col-xl-3">
              <div class="view zoom overlay z-depth-1 rounded mb-3 mb-md-0">
                <img class="img-fluid w-100"
                  src="{{ asset('storage/images/'.$myCart['product']['image']) }}" alt="Sample">
             
              </div>
            </div>
            <div class="col-md-7 col-lg-9 col-xl-9">
              <div>
                <div class="d-flex justify-content-between">
                  <div>
                    <h5>Blue denim shirt</h5>
                    <p class="mb-3 text-muted text-uppercase small">Shirt - blue</p>
                    <p class="mb-2 text-muted text-uppercase small">Color: blue</p>
                    <p class="mb-3 text-muted text-uppercase small">Size: M</p>
                  </div>
                  <div>
                    <div class="def-number-input number-input safari_only mb-0 w-100">
                      <button onclick="this.parentNode.querySelector('input[type=number]').stepDown()"
                        class=" btn  btn-sm btn-danger minus decrease">-</button>
                      <input class="quantity" min="0" name="quantity" value="1" type="number">
                      <button onclick="this.parentNode.querySelector('input[type=number]').stepUp()"
                        class=" btn btn-sm btn-primary plus increase">+</button>
                    </div>
                    
                  </div>
                </div>
                <div class="d-flex justify-content-between align-items-center">
                  <div>
                    <a onclick="remove({{$myCart['id']}},{{ Auth::id() }})" type="button" class="card-link-secondary small text-uppercase mr-3"><i class="bi bi-trash "></i> Remove item </a>
                   
                  </div>
                  <p class="mb-0"><span><strong id="summary">RM {{$myCart['product']['price'] }}</strong></span></p class="mb-0">
                </div>
              </div>
            </div>
            <hr class="mb-4">
          </div>
          

          @endforeach
        
          <p class="text-primary mb-0"><i class="bi bi-info-circle"></i> Do not delay the purchase, adding
            items to your cart does not mean booking them.</p>

        </div>
      </div>
      <!-- Card -->

      <!-- Card -->
      <div class="mb-3 cart p-5">
        <div class="pt-4">

          <h5 class="mb-4">Expected shipping delivery</h5>

          <p class="mb-0"> Thu., 12.03. - Mon., 16.03.</p>
        </div>
      </div>
      <!-- Card -->

      <!-- Card -->
      <div class="mb-3 cart p-5">
        <div class="pt-4">

          <h5 class="mb-4">We accept</h5>

          <img class="mr-2" width="45px"
            src="https://mdbootstrap.com/wp-content/plugins/woocommerce-gateway-stripe/assets/images/visa.svg"
            alt="Visa">
          <img class="mr-2" width="45px"
            src="https://mdbootstrap.com/wp-content/plugins/woocommerce-gateway-stripe/assets/images/amex.svg"
            alt="American Express">
          <img class="mr-2" width="45px"
            src="https://mdbootstrap.com/wp-content/plugins/woocommerce-gateway-stripe/assets/images/mastercard.svg"
            alt="Mastercard">
          <img class="mr-2" width="45px"
            src="https://mdbootstrap.com/wp-content/plugins/woocommerce/includes/gateways/paypal/assets/images/paypal.png"
            alt="PayPal acceptance mark">
        </div>
      </div>
      <!-- Card -->

    </div>
    <!--Grid column-->

    <!--Grid column-->
    <div class="col-lg-4  ">

      <!-- Card -->
      <div class="mb-3  cart p-5">
        <div class="pt-4">

          <h5 class="mb-3">The total amount of</h5>

          <ul class="list-group list-group-flush">
            <li class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 pb-0">
              Temporary amount
              <span>$25.98</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center px-0">
              Shipping
              <span>Gratis</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 mb-3">
              <div>
                <strong>The total amount of</strong>
                <strong>
                  <p class="mb-0">(including VAT)</p>
                </strong>
              </div>
              <span><strong>$53.98</strong></span>
            </li>
          </ul>

          <button type="button" class="btn btn-warning btn-block">go to checkout</button>

        </div>
      </div>
      <!-- Card -->

      <!-- Card -->
      <div class="mb-3">
        <div class="pt-4">

          <a class="dark-grey-text d-flex justify-content-between" data-toggle="collapse" href="#collapseExample"
            aria-expanded="false" aria-controls="collapseExample">
            Add a discount code (optional)
            <span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-down" viewBox="0 0 16 16">
  <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"/>
</svg></span>
          </a>

          <div class="collapse" id="collapseExample">
            <div class="mt-3">
              <div class="md-form md-outline mb-0">
                <input type="text" id="discount-code" class="form-control font-weight-light"
                  placeholder="Enter discount code">
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Card -->

    </div>
    <!--Grid column-->

  </div>
  <!-- Grid row -->

</section>
<!--Section: Block Content-->

<script>
    function remove(id, userID) {
        var id = id
        $.ajax({
            url: "{{ route('remove.cart')}}",
            type: "post",
            dataType: 'json',
            data: {

                "_token": "{{ csrf_token() }}",
                cartID: id
            }

        }).done(function(data) {

            if (data.status == "error") {
                $('#alert').removeClass('d-none');
                $('#alert').addClass('alert-danger');
                $('#alert').html("gagal remove cart item");
            } else {

                $('#alert').removeClass('d-none');
                $('#alert').addClass('alert-success');
                $('#alert').html("Berjaya remove cart item");

                // getCountCartItem();
                $("#" + id + "").remove();

                getCountCartItem(userID);
                countElement();
            }


        });


    }

    function getCountCartItem(userID) {

        // var userId =  $('#authId').val();
        $.ajax({

            url: "{{ route('cartitem.count') }}",
            type: 'get',
            dataType: 'json',
            data: {
                userId: userID,
            }
        }).done(function(data) {


            $('#cartItemCount').text(data.count);
        })
    }

    function countElement() {
        var c = document.getElementById("container").childElementCount;
        var b = document.getElementById("container");



        if (c == 1) {
            b.innerHTML += "<h4 class='text-center' id='cartTiada'>Tiada Cart Item</h4>";

            $("#checkOut").remove();

        }

    }
</script>
@endsection