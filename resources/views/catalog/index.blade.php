@extends('layout.catalog')

@section('content')

<section class="bg-light p-5">
    <div class="container">

        <table class="table table-bordered">

            <thead>
                <tr>
                    <th>Product Image</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Price RM</th>
                    <th>Quantity</th>
                    <th>Available</th>
                </tr>
            </thead>
            <tbody>


                @foreach($products As $product)
                <tr>
                    <td><img src="{{ asset('images/'.$product->image)}}" width="200px" height="200px" alt=""></td>
                    <td>{{ $product->name}}</td>
                    <td>{{ $product->desc}}</td>
                    <td>{{ $product->price}}</td>
                </tr> 
                @endforeach



            </tbody>

        </table>

        {{$products->links()}}

    </div>
</section>

@endsection