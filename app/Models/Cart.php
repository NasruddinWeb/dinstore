<?php

namespace App\Models;

use App\Http\Controllers\CartItemController;
use App\Models\Cart as ModelsCart;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Cart extends Model
{
    use HasFactory;


    protected $fillable = [
        'product_id',
        'user_id',
        'quantity'

    ];

    public static function userCartItem(){

        if(Auth::check()){

                $userCartItem = Cart::with(['product' => function($query){
                        $query->select('id','name','desc','price','image');
                }])->where('user_Id',Auth::id())->orderBy('id','desc')->get()->toArray();
        }



        return $userCartItem;
    }

    public static function getCountCartItem($id){
        
        if(Auth::check()){
            $count = Cart::where('user_id',Auth::id())->count();

        }

        return $count;
    }

    public function product(){

        return $this->belongsTo(Product::class);
    }
}
