<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
  

    protected $fillable = [

        'name',
        'desc',
        'category_id',
        'price',
        'discount_id',
        'image',
        'quantity'

    ];

    // public  function cart(){
    //     return $this->belongsTo(Cart::class);
    // }
}
