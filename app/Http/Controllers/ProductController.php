<?php

namespace App\Http\Controllers;

use App\Models\Product;

use Facade\FlareClient\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;


class ProductController extends Controller
{
    //

    public function index()
    {

        // $productListing = Product::all();
        $productListing = Product::select('id','name','desc','price','image')->paginate(10);
        
        //  dd($productListing);
        return View('product.index',[
            'products' => $productListing
        ]);
    }

    public function store()
    {

        return view('product.store');
    }

    public function storepost(Request $request)
    {

        $validate_date = $request->validate([

            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

            'name' => 'required|min:5|max:255',
            'desc' => 'required|min:10|max:1000',
            'price' => 'required|numeric'


        ]);

        $imageName = time() . '.' . $request->image->extension();

        $request->image->move(public_path('images'), $imageName);

        Product::create([
            'image' => $imageName,
            'name' => $request->name,
            'desc' => $request->desc,
            'price' => $request->price
        ]);


        return back()->with('success', 'Product has been Added');
    }

    public function detail($id){

        // $product = Product::where('id', $id)->first();
        $product = Product::select('id','name','price','image','desc','quantity')->find($id);
    
        return View('product.detail', [
            'product' => $product
        ]);
    }

    public function destroy($id){

        $product = Product::findOrFail($id);

        $image_path = "images/".$product->image;

    

        //delete file dalam public directory

        if(File::exists($image_path)) {

            // dd($image_path);

            File::delete($image_path);
        }

       

        $product->delete();


       return redirect()->route('product-listing')->with('success', 'Your product has been deleted');

    }

    public function edit($id){

        $product = Product::select('id','name','desc','image','price')->find($id);

        return view('product.update', [
            'product' => $product
        ]);

        
    }

    
}
