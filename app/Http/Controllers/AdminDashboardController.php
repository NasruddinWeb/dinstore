<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminDashboardController extends Controller
{
    //

    public function catalog(){
        $product = DB::table('products')->paginate(10);


        return view('catalog.index',[
            'products' => $product
        ]);
        // dd($product);
    }
}
