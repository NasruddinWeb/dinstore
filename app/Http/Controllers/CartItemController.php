<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Product;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;

class CartItemController extends Controller
{
    //

    public function addToCart(Request $request){


        //check Cart 
        $checkCart = Cart::where('Product_id',$request->productId)->count();
        //check availabe Quantity

        $productQuantity = Product::select('quantity')
                        ->where('id',$request->productId)
                        ->get();

        
       

      
        // dd($checkCart);
        if($checkCart > 0){

            $reponse_array = [
                'status' => 'error',
                'type' => 'Product Already exist in Cart'
            ];
        }elseif($request->quantity > $productQuantity[0]['quantity']){

            $reponse_array = [
                'status' => 'error',
                'type' => 'Quantity available only '.$productQuantity[0]['quantity']
            ];
        }
        
        else{
            $reponse_array = [
                'status' => 'success',
                'type' => 'Product Alreay Added in Cart',
               

            ];

            Cart::create([

                'product_id' => $request->productId,
                'user_id' => $request->userId,
                'quantity' => $request->quantity

    
            ]);

            //deduct quantity dalam product

            $product = Product::find($request->productId);

            // dd((int)$productQuantity[0]['quantity']);


            $product->quantity = $product->quantity -  $request->quantity ;

            $product->save();



           
        }

        return json_encode($reponse_array);
      
    }


    public function myCart($id){

        $userCartItem = Cart::userCartItem();

        // dd($userCartItem);
        
    //   foreach  
        return View('cart.mycart',[
            "cart" => $userCartItem
        ]);

        // dd($cartItem);

    }

    public function removeCart(Request $request){

        $cartItemToRemove = Cart::find($request->cartID);

      try{

        $cartItemToRemove->delete();
        $data = [
            'status' => "success"
        ];

      }catch(Exception $e){

        $data = [
            'status' => "error"
        ];

      }

      return json_encode($data);
    }

    public function getCartItemCount(Request $request){

        $cartItemCount = Cart::getCountCartItem($request->userId);

        $data = [
             'count' => $cartItemCount,
        ];


        return json_encode($data);
    }
}
