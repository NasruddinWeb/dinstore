<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        // $filPath = storage_path('images');
        return [
            'name' => $this->faker->sentence(),
            'desc' => $this->faker->paragraphs(2,true),
            'price' => $this->faker->randomFloat(2,10,300),
            'quantity' => $this->faker->randomDigit(),
            'image' => $this->faker->image('public/storage/images',360,360,null,false),
            'created_at' => $this->faker->date('Y-m-d H:i:s'),
            'updated_at' => $this->faker->date('Y-m-d H:i:s')


        ];
    }
}
